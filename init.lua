local function coord(x, ch)
	if x > 0 then
		return ""..x..ch[1]
	else
		return ""..x..ch[2]	
	end
end

for x = -2, 2 do
	for z = -2, 2 do
		minetest.register_node(string.format("worldmap:%d_%d", x+2, z+2),{
		    description = "World Map "..coord(x*12-6, {"E","W"})..":"..coord(x*12+6, {"E","W"}).." "..coord(z*12-6, {"N","S"})..":"..coord(z*12+6, {"N","S"}),
		    drawtype = "signlike",
		    visual_scale=3.0,
		    wield_image = string.format("map_%d_%d.png", x, z),
		    inventory_image= string.format("map_%d_%d.png", x, z),
		    tiles = { string.format("map_%d_%d.png", x, z) },
		    sunlight_propagates = true,
		    walkable = false,
		    paramtype = "light",
		    paramtype2 = "wallmounted",
		    light_source=10,
		    selection_box = {
			type = "wallmounted"
		    },
		    groups = {choppy=2,dig_immediate=3,attached_node=1, picture=1},
		    legacy_wallmounted = true,
		})
	end
end
